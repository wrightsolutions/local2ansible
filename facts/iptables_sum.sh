#!/bin/dash
#   Copyright 2013 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#   On Debian systems, the complete text of the Apache license can be found in
#   /usr/share/common-licenses/Apache-2.0

IPTOUT_=""
# Debian and rpm based systems now store archive in /usr/lib/locale
MD5RES_=$(/sbin/iptables -n -L | md5sum | \
	egrep 'f2384cfbed4d4fb64061368c4128d7ea ')
MD5RC_=$?
printf "{\n"
if [ ${MD5RC_} -eq 0 ]; then
        printf '"summary" : "default"'
        printf "\n"
	printf '"shortened" : "23844128"'
else
        printf '"summary" : "nondefault"'
fi
printf "\n}\n"
