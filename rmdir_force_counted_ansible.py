#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2013, Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from sys import exit
from os import path,sep
from shutil import rmtree
from glob import glob

""" Remove directories from ~ or /root/ that match some given glob.
Tested against Python 2.7.3 """

try:
    from argparse import ArgumentParser as ArgParser
    import subprocess
except ImportError as e:
    exit(131)

VERSION='0.3'
DESCRIPTION="""Rmdir Example args: 'tilde' 50 '.mozilla/firefox/*/Cache/*'
For local directories existing as follows:
/home/someuser/tmp1
/home/someuser/tmp2
The Example args: 'tilde' 50 'tmp*'
...would result in both directories being rm -f'd
The directory count is the count at the top level only.
Count is formed from process using Python glob() but could be changed.
"""

def parser_initialise():
    par = None
    par = ArgParser(description=DESCRIPTION)
    par.add_argument('-v', '--version', help='Show version',
                     action='store_true')
    par.add_argument('-s', '--stem', help="Home ('tilde' or /home/someuser) or ('root' or '/root/')",
                     action='store', dest='homestem', default='~')
    par.add_argument('-m', '--maximum', help='Maximum number of directories / subdirectories to rmdir',
                     action='store', dest='maximum_rmdir_count', type=int, default=100)
    par.add_argument('-g', '--glob', help='Glob pattern to append to ~ or /root/ (normally ends in *)',
                     action='store', dest='glob_pattern', default='.ansible/tmp/*')
    par.add_argument('-z', '--zerorcminimum', help='Minimum number of directories / subdirectories to achieve 0 rc',
                     action='store', dest='minimum_rmdir_count', type=int, default=1)
    # -z 0 would mean that even if no directories removed, the program would still exit 0
    return par

parser = parser_initialise()
if parser is None:
    exit(132)
    
args = parser.parse_args()

if args.version:
    print(VERSION)
    exit(0)

if args.maximum_rmdir_count < 1 or len(args.glob_pattern) < 2:
    parser.error('Unworkable arguments or other args issue')
    exit(133)

if args.homestem == chr(126):
    homestem = args.homestem
elif args.homestem == 'tilde':
    homestem == chr(126)
elif args.homestem.startswith(sep):
    homestem = args.homestem
else:
    homestem = "{0}{1}".format(sep,args.homestem)
#print(homestem)

dirs = []
dirs_count = 0
try:
    home_expanded = path.expanduser(homestem)
    glob_formed = "{0}{1}{2}".format(home_expanded,sep,args.glob_pattern)
    #print(glob_formed)
    glob_matches = glob(glob_formed)
    for listpath in glob_matches:
        if path.isdir(listpath):
            dirs.append(listpath)
            dirs_count += 1
except Exception as e:
    dirs_count = 0

#print(dirs)

if dirs_count < 1:
    if args.minimum_rmdir_count < 1:
        exit(0)
    else:
        exit(140)
elif dirs_count >= args.minimum_rmdir_count:
    for dir_to_remove in dirs:
        if dir_to_remove.startswith(sep) and len(dir_to_remove) > 1:
            # The if ... and ... above is just extra safety
            #print(dir_to_remove)
            rmtree(dir_to_remove,False) # Set second argument to True if you prefer
    exit(0)
else:
    exit(140)





